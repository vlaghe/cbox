#!/bin/bash

dialog --title "Setting up timezone, locale and networkManager..."
# Timezone stuff
TZuser=$(cat tzfinal.tmp)
ln -sf /usr/share/zoneinfo/$TZuser /etc/localtime
hwclock --systohc

# Setup locale
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US ISO-8859-1" >> /etc/locale.gen
locale-gen

# Install and enable networkmanager service
pacman --noconfirm --needed -S networkmanager
systemctl enable NetworkManager
systemctl start NetworkManager

# Install & configure grub
pacman --noconfirm --needed -S grub && grub-install --target=i386-pc /dev/sda && grub-mkconfig -o /boot/grub/grub.cfg

pacman --noconfirm --needed -S dialog
larbs() { 
    curl -LO "https://gitlab.com/vlaghe/cbox/-/raw/master/.local/share/init_install/larbs.sh" && \ 
        bash larbs.sh -r "https://gitlab.com/vlaghe/cbox.git" -p "https://gitlab.com/vlaghe/cbox/-/raw/master/.local/share/pkgs.csv"
}

dialog --title "Install Vlaghe's Rice" --yesno "This install script will easily let you access the cyber-box which automatically install a full Arch Linux i3-gaps desktop environment.\n\nIf you'd like to install this, select yes, otherwise select no.\n\nVlaghe"  15 60 && larbs # && su rooster -c "./after.sh"

# Clean-Up
rm larbs.sh after.sh
