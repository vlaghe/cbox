#/bin/bash

# Set the ZSH env if it's not already set
if [ -z ${ZSH} ]; then
    export ZSH="${XDG_CONFIG_HOME:-$HOME/.local/share}/oh_my_zsh"
fi

# Install ohmyzsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# Remove .zshrc if existent
[ -f "$HOME/.zshrc" ] && rm $HOME/.zshrc

# Install required plugins (syntax highlighting)
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH:-$HOME/.local/share/oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Install tmux plugin manager
git clone https://github.com/tmux-plugins/tpm "${XDG_DATA_HOME:-$HOME/.local/share}/tmux/tpm"

# Install powerline fonts
git clone https://github.com/powerline/fonts.git --depth=1 && cd fonts && ./install.sh && cd .. && rm -rf fonts

# Install neovim plugin manager (vim-plug)
sh -c 'curl -fLo "${XDG_CONFIG_HOME:-$HOME/.config}"/nvim/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Install vim plugins
nvim --headless +PlugInstall +qa

# Setup vim cyberpunk theme
nvim_airtheme_path="${XDG_CONFIG_HOME:-$HOME/.config}/nvim/themes/cyberpunk_air.vim"
[ -f $nvim_airtheme_path ] && cp $nvim_airtheme_path "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plugged/vim-airline-themes/autoload/airline/themes/cyberpunk.vim"


# BlackArch tools



